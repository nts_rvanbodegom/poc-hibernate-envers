import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PocHibernateEnversTestModule } from '../../../test.module';
import { ExtendedStudentComponent } from 'app/entities/extended-student/extended-student.component';
import { ExtendedStudentService } from 'app/entities/extended-student/extended-student.service';
import { ExtendedStudent } from 'app/shared/model/extended-student.model';

describe('Component Tests', () => {
  describe('ExtendedStudent Management Component', () => {
    let comp: ExtendedStudentComponent;
    let fixture: ComponentFixture<ExtendedStudentComponent>;
    let service: ExtendedStudentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PocHibernateEnversTestModule],
        declarations: [ExtendedStudentComponent],
        providers: []
      })
        .overrideTemplate(ExtendedStudentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExtendedStudentComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedStudentService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ExtendedStudent(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.extendedStudents[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
