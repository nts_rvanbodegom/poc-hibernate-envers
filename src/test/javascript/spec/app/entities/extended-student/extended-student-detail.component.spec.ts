import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PocHibernateEnversTestModule } from '../../../test.module';
import { ExtendedStudentDetailComponent } from 'app/entities/extended-student/extended-student-detail.component';
import { ExtendedStudent } from 'app/shared/model/extended-student.model';

describe('Component Tests', () => {
  describe('ExtendedStudent Management Detail Component', () => {
    let comp: ExtendedStudentDetailComponent;
    let fixture: ComponentFixture<ExtendedStudentDetailComponent>;
    const route = ({ data: of({ extendedStudent: new ExtendedStudent(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PocHibernateEnversTestModule],
        declarations: [ExtendedStudentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExtendedStudentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedStudentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.extendedStudent).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
