import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PocHibernateEnversTestModule } from '../../../test.module';
import { ExtendedStudentDeleteDialogComponent } from 'app/entities/extended-student/extended-student-delete-dialog.component';
import { ExtendedStudentService } from 'app/entities/extended-student/extended-student.service';

describe('Component Tests', () => {
  describe('ExtendedStudent Management Delete Component', () => {
    let comp: ExtendedStudentDeleteDialogComponent;
    let fixture: ComponentFixture<ExtendedStudentDeleteDialogComponent>;
    let service: ExtendedStudentService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PocHibernateEnversTestModule],
        declarations: [ExtendedStudentDeleteDialogComponent]
      })
        .overrideTemplate(ExtendedStudentDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedStudentDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedStudentService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
