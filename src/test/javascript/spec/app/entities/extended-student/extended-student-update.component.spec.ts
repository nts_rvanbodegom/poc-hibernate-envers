import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PocHibernateEnversTestModule } from '../../../test.module';
import { ExtendedStudentUpdateComponent } from 'app/entities/extended-student/extended-student-update.component';
import { ExtendedStudentService } from 'app/entities/extended-student/extended-student.service';
import { ExtendedStudent } from 'app/shared/model/extended-student.model';

describe('Component Tests', () => {
  describe('ExtendedStudent Management Update Component', () => {
    let comp: ExtendedStudentUpdateComponent;
    let fixture: ComponentFixture<ExtendedStudentUpdateComponent>;
    let service: ExtendedStudentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PocHibernateEnversTestModule],
        declarations: [ExtendedStudentUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExtendedStudentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExtendedStudentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedStudentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedStudent(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedStudent();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
