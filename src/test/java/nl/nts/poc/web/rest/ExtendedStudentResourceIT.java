package nl.nts.poc.web.rest;

import nl.nts.poc.PocHibernateEnversApp;
import nl.nts.poc.domain.ExtendedStudent;
import nl.nts.poc.repository.ExtendedStudentRepository;
import nl.nts.poc.service.ExtendedStudentService;
import nl.nts.poc.service.dto.ExtendedStudentDTO;
import nl.nts.poc.service.mapper.ExtendedStudentMapper;
import nl.nts.poc.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static nl.nts.poc.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ExtendedStudentResource} REST controller.
 */
@SpringBootTest(classes = PocHibernateEnversApp.class)
public class ExtendedStudentResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    @Autowired
    private ExtendedStudentRepository extendedStudentRepository;

    @Autowired
    private ExtendedStudentMapper extendedStudentMapper;

    @Autowired
    private ExtendedStudentService extendedStudentService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExtendedStudentMockMvc;

    private ExtendedStudent extendedStudent;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedStudent createEntity(EntityManager em) {
        ExtendedStudent extendedStudent = new ExtendedStudent()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME);
        return extendedStudent;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedStudent createUpdatedEntity(EntityManager em) {
        ExtendedStudent extendedStudent = new ExtendedStudent()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME);
        return extendedStudent;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExtendedStudentResource extendedStudentResource = new ExtendedStudentResource(extendedStudentService);
        this.restExtendedStudentMockMvc = MockMvcBuilders.standaloneSetup(extendedStudentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        extendedStudent = createEntity(em);
    }

    @Test
    @Transactional
    public void createExtendedStudent() throws Exception {
        int databaseSizeBeforeCreate = extendedStudentRepository.findAll().size();

        // Create the ExtendedStudent
        ExtendedStudentDTO extendedStudentDTO = extendedStudentMapper.toDto(extendedStudent);
        restExtendedStudentMockMvc.perform(post("/api/extended-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedStudentDTO)))
            .andExpect(status().isCreated());

        // Validate the ExtendedStudent in the database
        List<ExtendedStudent> extendedStudentList = extendedStudentRepository.findAll();
        assertThat(extendedStudentList).hasSize(databaseSizeBeforeCreate + 1);
        ExtendedStudent testExtendedStudent = extendedStudentList.get(extendedStudentList.size() - 1);
        assertThat(testExtendedStudent.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testExtendedStudent.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
    }

    @Test
    @Transactional
    public void createExtendedStudentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = extendedStudentRepository.findAll().size();

        // Create the ExtendedStudent with an existing ID
        extendedStudent.setId(1L);
        ExtendedStudentDTO extendedStudentDTO = extendedStudentMapper.toDto(extendedStudent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExtendedStudentMockMvc.perform(post("/api/extended-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedStudentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedStudent in the database
        List<ExtendedStudent> extendedStudentList = extendedStudentRepository.findAll();
        assertThat(extendedStudentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllExtendedStudents() throws Exception {
        // Initialize the database
        extendedStudentRepository.saveAndFlush(extendedStudent);

        // Get all the extendedStudentList
        restExtendedStudentMockMvc.perform(get("/api/extended-students?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedStudent.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())));
    }

    @Test
    @Transactional
    public void getExtendedStudent() throws Exception {
        // Initialize the database
        extendedStudentRepository.saveAndFlush(extendedStudent);

        // Get the extendedStudent
        restExtendedStudentMockMvc.perform(get("/api/extended-students/{id}", extendedStudent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(extendedStudent.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExtendedStudent() throws Exception {
        // Get the extendedStudent
        restExtendedStudentMockMvc.perform(get("/api/extended-students/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExtendedStudent() throws Exception {
        // Initialize the database
        extendedStudentRepository.saveAndFlush(extendedStudent);

        int databaseSizeBeforeUpdate = extendedStudentRepository.findAll().size();

        // Update the extendedStudent
        ExtendedStudent updatedExtendedStudent = extendedStudentRepository.findById(extendedStudent.getId()).get();
        // Disconnect from session so that the updates on updatedExtendedStudent are not directly saved in db
        em.detach(updatedExtendedStudent);
        updatedExtendedStudent
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME);
        ExtendedStudentDTO extendedStudentDTO = extendedStudentMapper.toDto(updatedExtendedStudent);

        restExtendedStudentMockMvc.perform(put("/api/extended-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedStudentDTO)))
            .andExpect(status().isOk());

        // Validate the ExtendedStudent in the database
        List<ExtendedStudent> extendedStudentList = extendedStudentRepository.findAll();
        assertThat(extendedStudentList).hasSize(databaseSizeBeforeUpdate);
        ExtendedStudent testExtendedStudent = extendedStudentList.get(extendedStudentList.size() - 1);
        assertThat(testExtendedStudent.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testExtendedStudent.getLastName()).isEqualTo(UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingExtendedStudent() throws Exception {
        int databaseSizeBeforeUpdate = extendedStudentRepository.findAll().size();

        // Create the ExtendedStudent
        ExtendedStudentDTO extendedStudentDTO = extendedStudentMapper.toDto(extendedStudent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExtendedStudentMockMvc.perform(put("/api/extended-students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedStudentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedStudent in the database
        List<ExtendedStudent> extendedStudentList = extendedStudentRepository.findAll();
        assertThat(extendedStudentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteExtendedStudent() throws Exception {
        // Initialize the database
        extendedStudentRepository.saveAndFlush(extendedStudent);

        int databaseSizeBeforeDelete = extendedStudentRepository.findAll().size();

        // Delete the extendedStudent
        restExtendedStudentMockMvc.perform(delete("/api/extended-students/{id}", extendedStudent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExtendedStudent> extendedStudentList = extendedStudentRepository.findAll();
        assertThat(extendedStudentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtendedStudent.class);
        ExtendedStudent extendedStudent1 = new ExtendedStudent();
        extendedStudent1.setId(1L);
        ExtendedStudent extendedStudent2 = new ExtendedStudent();
        extendedStudent2.setId(extendedStudent1.getId());
        assertThat(extendedStudent1).isEqualTo(extendedStudent2);
        extendedStudent2.setId(2L);
        assertThat(extendedStudent1).isNotEqualTo(extendedStudent2);
        extendedStudent1.setId(null);
        assertThat(extendedStudent1).isNotEqualTo(extendedStudent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtendedStudentDTO.class);
        ExtendedStudentDTO extendedStudentDTO1 = new ExtendedStudentDTO();
        extendedStudentDTO1.setId(1L);
        ExtendedStudentDTO extendedStudentDTO2 = new ExtendedStudentDTO();
        assertThat(extendedStudentDTO1).isNotEqualTo(extendedStudentDTO2);
        extendedStudentDTO2.setId(extendedStudentDTO1.getId());
        assertThat(extendedStudentDTO1).isEqualTo(extendedStudentDTO2);
        extendedStudentDTO2.setId(2L);
        assertThat(extendedStudentDTO1).isNotEqualTo(extendedStudentDTO2);
        extendedStudentDTO1.setId(null);
        assertThat(extendedStudentDTO1).isNotEqualTo(extendedStudentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(extendedStudentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(extendedStudentMapper.fromId(null)).isNull();
    }
}
