package nl.nts.poc.repository;

import nl.nts.poc.domain.ExtendedStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ExtendedStudent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtendedStudentRepository extends JpaRepository<ExtendedStudent, Long> {

}
