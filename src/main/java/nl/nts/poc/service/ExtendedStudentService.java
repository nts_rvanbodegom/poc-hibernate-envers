package nl.nts.poc.service;

import nl.nts.poc.service.dto.ExtendedStudentDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link nl.nts.poc.domain.ExtendedStudent}.
 */
public interface ExtendedStudentService {

    /**
     * Save a extendedStudent.
     *
     * @param extendedStudentDTO the entity to save.
     * @return the persisted entity.
     */
    ExtendedStudentDTO save(ExtendedStudentDTO extendedStudentDTO);

    /**
     * Get all the extendedStudents.
     *
     * @return the list of entities.
     */
    List<ExtendedStudentDTO> findAll();


    /**
     * Get the "id" extendedStudent.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExtendedStudentDTO> findOne(Long id);

    /**
     * Delete the "id" extendedStudent.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
