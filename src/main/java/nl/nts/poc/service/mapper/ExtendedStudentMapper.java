package nl.nts.poc.service.mapper;

import nl.nts.poc.domain.ExtendedStudent;
import nl.nts.poc.service.dto.ExtendedStudentDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link ExtendedStudent} and its DTO {@link ExtendedStudentDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ExtendedStudentMapper extends EntityMapper<ExtendedStudentDTO, ExtendedStudent> {


    default ExtendedStudent fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExtendedStudent extendedStudent = new ExtendedStudent();
        extendedStudent.setId(id);
        return extendedStudent;
    }
}
