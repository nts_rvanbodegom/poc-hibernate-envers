package nl.nts.poc.service.mapper;

import nl.nts.poc.domain.Student;
import nl.nts.poc.service.dto.StudentDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Student} and its DTO {@link StudentDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StudentMapper extends EntityMapper<StudentDTO, Student> {


    default Student fromId(Long id) {
        if (id == null) {
            return null;
        }
        Student student = new Student();
        student.setId(id);
        return student;
    }
}
