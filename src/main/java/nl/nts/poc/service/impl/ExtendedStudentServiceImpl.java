package nl.nts.poc.service.impl;

import nl.nts.poc.domain.ExtendedStudent;
import nl.nts.poc.repository.ExtendedStudentRepository;
import nl.nts.poc.service.ExtendedStudentService;
import nl.nts.poc.service.dto.ExtendedStudentDTO;
import nl.nts.poc.service.mapper.ExtendedStudentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ExtendedStudent}.
 */
@Service
@Transactional
public class ExtendedStudentServiceImpl implements ExtendedStudentService {

    private final Logger log = LoggerFactory.getLogger(ExtendedStudentServiceImpl.class);

    private final ExtendedStudentRepository extendedStudentRepository;

    private final ExtendedStudentMapper extendedStudentMapper;

    public ExtendedStudentServiceImpl(ExtendedStudentRepository extendedStudentRepository, ExtendedStudentMapper extendedStudentMapper) {
        this.extendedStudentRepository = extendedStudentRepository;
        this.extendedStudentMapper = extendedStudentMapper;
    }

    /**
     * Save a extendedStudent.
     *
     * @param extendedStudentDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ExtendedStudentDTO save(ExtendedStudentDTO extendedStudentDTO) {
        log.debug("Request to save ExtendedStudent : {}", extendedStudentDTO);
        ExtendedStudent extendedStudent = extendedStudentMapper.toEntity(extendedStudentDTO);
        extendedStudent = extendedStudentRepository.save(extendedStudent);
        return extendedStudentMapper.toDto(extendedStudent);
    }

    /**
     * Get all the extendedStudents.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<ExtendedStudentDTO> findAll() {
        log.debug("Request to get all ExtendedStudents");
        return extendedStudentRepository.findAll().stream()
            .map(extendedStudentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one extendedStudent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ExtendedStudentDTO> findOne(Long id) {
        log.debug("Request to get ExtendedStudent : {}", id);
        return extendedStudentRepository.findById(id)
            .map(extendedStudentMapper::toDto);
    }

    /**
     * Delete the extendedStudent by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExtendedStudent : {}", id);
        extendedStudentRepository.deleteById(id);
    }
}
