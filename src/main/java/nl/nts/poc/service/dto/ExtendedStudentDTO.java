package nl.nts.poc.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link nl.nts.poc.domain.ExtendedStudent} entity.
 */
public class ExtendedStudentDTO implements Serializable {

    private Long id;

    private String firstName;

    private String lastName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExtendedStudentDTO extendedStudentDTO = (ExtendedStudentDTO) o;
        if (extendedStudentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), extendedStudentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExtendedStudentDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            "}";
    }
}
