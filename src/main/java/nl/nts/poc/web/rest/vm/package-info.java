/**
 * View Models used by Spring MVC REST controllers.
 */
package nl.nts.poc.web.rest.vm;
