package nl.nts.poc.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import nl.nts.poc.service.ExtendedStudentService;
import nl.nts.poc.service.dto.ExtendedStudentDTO;
import nl.nts.poc.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link nl.nts.poc.domain.ExtendedStudent}.
 */
@RestController
@RequestMapping("/api")
public class ExtendedStudentResource {

    private static final String ENTITY_NAME = "extendedStudent";
    private final Logger log = LoggerFactory.getLogger(ExtendedStudentResource.class);
    private final ExtendedStudentService extendedStudentService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ExtendedStudentResource(ExtendedStudentService extendedStudentService) {
        this.extendedStudentService = extendedStudentService;
    }

    /**
     * {@code POST  /extended-students} : Create a new extendedStudent.
     *
     * @param extendedStudentDTO the extendedStudentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new extendedStudentDTO, or with status {@code 400 (Bad Request)} if the extendedStudent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/extended-students")
    public ResponseEntity<ExtendedStudentDTO> createExtendedStudent(@RequestBody ExtendedStudentDTO extendedStudentDTO) throws URISyntaxException {
        log.debug("REST request to save ExtendedStudent : {}", extendedStudentDTO);
        if (extendedStudentDTO.getId() != null) {
            throw new BadRequestAlertException("A new extendedStudent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExtendedStudentDTO result = extendedStudentService.save(extendedStudentDTO);
        return ResponseEntity.created(new URI("/api/extended-students/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /extended-students} : Updates an existing extendedStudent.
     *
     * @param extendedStudentDTO the extendedStudentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedStudentDTO,
     * or with status {@code 400 (Bad Request)} if the extendedStudentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedStudentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/extended-students")
    public ResponseEntity<ExtendedStudentDTO> updateExtendedStudent(@RequestBody ExtendedStudentDTO extendedStudentDTO) throws URISyntaxException {
        log.debug("REST request to update ExtendedStudent : {}", extendedStudentDTO);
        if (extendedStudentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ExtendedStudentDTO result = extendedStudentService.save(extendedStudentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, extendedStudentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /extended-students} : get all the extendedStudents.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of extendedStudents in body.
     */
    @GetMapping("/extended-students")
    public List<ExtendedStudentDTO> getAllExtendedStudents() {
        log.debug("REST request to get all ExtendedStudents");
        return extendedStudentService.findAll();
    }

    /**
     * {@code GET  /extended-students/:id} : get the "id" extendedStudent.
     *
     * @param id the id of the extendedStudentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedStudentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/extended-students/{id}")
    public ResponseEntity<ExtendedStudentDTO> getExtendedStudent(@PathVariable Long id) {
        log.debug("REST request to get ExtendedStudent : {}", id);
        Optional<ExtendedStudentDTO> extendedStudentDTO = extendedStudentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(extendedStudentDTO);
    }

    /**
     * {@code DELETE  /extended-students/:id} : delete the "id" extendedStudent.
     *
     * @param id the id of the extendedStudentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/extended-students/{id}")
    public ResponseEntity<Void> deleteExtendedStudent(@PathVariable Long id) {
        log.debug("REST request to delete ExtendedStudent : {}", id);
        extendedStudentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
