package nl.nts.poc.domain;

import org.hibernate.envers.RevisionListener;

public class CustomListener implements RevisionListener {
    public void newRevision(Object revisionEntity) {
        String userName = "system";
        CustomRevEntity customRevEntity = (CustomRevEntity) revisionEntity;
        // username
        if (true) { // TODO if (check if a user is logged id) & get the actual username
            userName = "logged_in_user";
        }
        customRevEntity.setUsername(userName);

    }

}
