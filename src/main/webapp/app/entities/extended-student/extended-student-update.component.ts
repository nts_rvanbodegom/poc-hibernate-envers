import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ExtendedStudent, IExtendedStudent } from 'app/shared/model/extended-student.model';
import { ExtendedStudentService } from './extended-student.service';

@Component({
  selector: 'jhi-extended-student-update',
  templateUrl: './extended-student-update.component.html'
})
export class ExtendedStudentUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: []
  });

  constructor(
    protected extendedStudentService: ExtendedStudentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ extendedStudent }) => {
      this.updateForm(extendedStudent);
    });
  }

  updateForm(extendedStudent: IExtendedStudent) {
    this.editForm.patchValue({
      id: extendedStudent.id,
      firstName: extendedStudent.firstName,
      lastName: extendedStudent.lastName
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const extendedStudent = this.createFromForm();
    if (extendedStudent.id !== undefined) {
      this.subscribeToSaveResponse(this.extendedStudentService.update(extendedStudent));
    } else {
      this.subscribeToSaveResponse(this.extendedStudentService.create(extendedStudent));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExtendedStudent>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  private createFromForm(): IExtendedStudent {
    return {
      ...new ExtendedStudent(),
      id: this.editForm.get(['id']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value
    };
  }
}
