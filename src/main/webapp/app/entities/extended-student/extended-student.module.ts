import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PocHibernateEnversSharedModule } from 'app/shared/shared.module';
import { ExtendedStudentComponent } from './extended-student.component';
import { ExtendedStudentDetailComponent } from './extended-student-detail.component';
import { ExtendedStudentUpdateComponent } from './extended-student-update.component';
import { ExtendedStudentDeleteDialogComponent, ExtendedStudentDeletePopupComponent } from './extended-student-delete-dialog.component';
import { extendedStudentPopupRoute, extendedStudentRoute } from './extended-student.route';

const ENTITY_STATES = [...extendedStudentRoute, ...extendedStudentPopupRoute];

@NgModule({
  imports: [PocHibernateEnversSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ExtendedStudentComponent,
    ExtendedStudentDetailComponent,
    ExtendedStudentUpdateComponent,
    ExtendedStudentDeleteDialogComponent,
    ExtendedStudentDeletePopupComponent
  ],
  entryComponents: [ExtendedStudentDeleteDialogComponent]
})
export class PocHibernateEnversExtendedStudentModule {}
