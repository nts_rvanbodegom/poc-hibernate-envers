import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExtendedStudent } from 'app/shared/model/extended-student.model';
import { ExtendedStudentService } from './extended-student.service';

@Component({
  selector: 'jhi-extended-student-delete-dialog',
  templateUrl: './extended-student-delete-dialog.component.html'
})
export class ExtendedStudentDeleteDialogComponent {
  extendedStudent: IExtendedStudent;

  constructor(
    protected extendedStudentService: ExtendedStudentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.extendedStudentService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'extendedStudentListModification',
        content: 'Deleted an extendedStudent'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-extended-student-delete-popup',
  template: ''
})
export class ExtendedStudentDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedStudent }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ExtendedStudentDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.extendedStudent = extendedStudent;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/extended-student', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/extended-student', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
