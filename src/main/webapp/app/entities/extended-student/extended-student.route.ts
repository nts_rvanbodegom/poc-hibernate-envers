import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ExtendedStudent, IExtendedStudent } from 'app/shared/model/extended-student.model';
import { ExtendedStudentService } from './extended-student.service';
import { ExtendedStudentComponent } from './extended-student.component';
import { ExtendedStudentDetailComponent } from './extended-student-detail.component';
import { ExtendedStudentUpdateComponent } from './extended-student-update.component';
import { ExtendedStudentDeletePopupComponent } from './extended-student-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class ExtendedStudentResolve implements Resolve<IExtendedStudent> {
  constructor(private service: ExtendedStudentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExtendedStudent> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ExtendedStudent>) => response.ok),
        map((extendedStudent: HttpResponse<ExtendedStudent>) => extendedStudent.body)
      );
    }
    return of(new ExtendedStudent());
  }
}

export const extendedStudentRoute: Routes = [
  {
    path: '',
    component: ExtendedStudentComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pocHibernateEnversApp.extendedStudent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExtendedStudentDetailComponent,
    resolve: {
      extendedStudent: ExtendedStudentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pocHibernateEnversApp.extendedStudent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExtendedStudentUpdateComponent,
    resolve: {
      extendedStudent: ExtendedStudentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pocHibernateEnversApp.extendedStudent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExtendedStudentUpdateComponent,
    resolve: {
      extendedStudent: ExtendedStudentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pocHibernateEnversApp.extendedStudent.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const extendedStudentPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ExtendedStudentDeletePopupComponent,
    resolve: {
      extendedStudent: ExtendedStudentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pocHibernateEnversApp.extendedStudent.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
