import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IExtendedStudent } from 'app/shared/model/extended-student.model';

type EntityResponseType = HttpResponse<IExtendedStudent>;
type EntityArrayResponseType = HttpResponse<IExtendedStudent[]>;

@Injectable({ providedIn: 'root' })
export class ExtendedStudentService {
  public resourceUrl = SERVER_API_URL + 'api/extended-students';

  constructor(protected http: HttpClient) {}

  create(extendedStudent: IExtendedStudent): Observable<EntityResponseType> {
    return this.http.post<IExtendedStudent>(this.resourceUrl, extendedStudent, { observe: 'response' });
  }

  update(extendedStudent: IExtendedStudent): Observable<EntityResponseType> {
    return this.http.put<IExtendedStudent>(this.resourceUrl, extendedStudent, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IExtendedStudent>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExtendedStudent[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
