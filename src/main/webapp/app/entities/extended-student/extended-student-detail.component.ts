import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExtendedStudent } from 'app/shared/model/extended-student.model';

@Component({
  selector: 'jhi-extended-student-detail',
  templateUrl: './extended-student-detail.component.html'
})
export class ExtendedStudentDetailComponent implements OnInit {
  extendedStudent: IExtendedStudent;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedStudent }) => {
      this.extendedStudent = extendedStudent;
    });
  }

  previousState() {
    window.history.back();
  }
}
