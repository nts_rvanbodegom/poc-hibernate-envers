import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IExtendedStudent } from 'app/shared/model/extended-student.model';
import { AccountService } from 'app/core/auth/account.service';
import { ExtendedStudentService } from './extended-student.service';

@Component({
  selector: 'jhi-extended-student',
  templateUrl: './extended-student.component.html'
})
export class ExtendedStudentComponent implements OnInit, OnDestroy {
  extendedStudents: IExtendedStudent[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected extendedStudentService: ExtendedStudentService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.extendedStudentService
      .query()
      .pipe(
        filter((res: HttpResponse<IExtendedStudent[]>) => res.ok),
        map((res: HttpResponse<IExtendedStudent[]>) => res.body)
      )
      .subscribe(
        (res: IExtendedStudent[]) => {
          this.extendedStudents = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInExtendedStudents();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IExtendedStudent) {
    return item.id;
  }

  registerChangeInExtendedStudents() {
    this.eventSubscriber = this.eventManager.subscribe('extendedStudentListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
