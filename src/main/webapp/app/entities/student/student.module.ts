import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PocHibernateEnversSharedModule } from 'app/shared/shared.module';
import { StudentComponent } from './student.component';
import { StudentDetailComponent } from './student-detail.component';
import { StudentUpdateComponent } from './student-update.component';
import { StudentDeleteDialogComponent, StudentDeletePopupComponent } from './student-delete-dialog.component';
import { studentPopupRoute, studentRoute } from './student.route';

const ENTITY_STATES = [...studentRoute, ...studentPopupRoute];

@NgModule({
  imports: [PocHibernateEnversSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    StudentComponent,
    StudentDetailComponent,
    StudentUpdateComponent,
    StudentDeleteDialogComponent,
    StudentDeletePopupComponent
  ],
  entryComponents: [StudentDeleteDialogComponent]
})
export class PocHibernateEnversStudentModule {}
