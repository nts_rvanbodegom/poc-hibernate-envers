import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PocHibernateEnversSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [PocHibernateEnversSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [PocHibernateEnversSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PocHibernateEnversSharedModule {
  static forRoot() {
    return {
      ngModule: PocHibernateEnversSharedModule
    };
  }
}
