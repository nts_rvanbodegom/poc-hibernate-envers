export interface IExtendedStudent {
  id?: number;
  firstName?: string;
  lastName?: string;
}

export class ExtendedStudent implements IExtendedStudent {
  constructor(public id?: number, public firstName?: string, public lastName?: string) {}
}
