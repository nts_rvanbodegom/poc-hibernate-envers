import { Moment } from 'moment';

export interface IStudent {
  id?: number;
  firstName?: string;
  lastName?: string;
  dateOfBirth?: Moment;
  placeOfBirth?: string;
}

export class Student implements IStudent {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public dateOfBirth?: Moment,
    public placeOfBirth?: string
  ) {}
}
