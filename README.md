# pocHibernateEnvers

This poc uses Hibernate Envers. https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#envers

This poc contains 2 entities, student and extended student. Both extend the AbstractAuditingEntity, which adds 4 columns in the domain.

Student has the @audited annotation, which also add a student_aud table to the database which keeps track of all the history of the table.

ExtendedStudent uses the @Audited(withModifiedFlag = true), which also adds a boolean field for each field to indicate whether or not the respective field was modified in that revision. According to several resources this doesn't add much overhead during the persist of such a row, but it does make retrieving (specific) record a lot more efficient.

The default revinfo has been customized (CustomRevEntity and CustomListener) to allow the addition of the username to each revision record.

For more info, refer to:
https://hibernate.org/orm/envers/
https://thoughts-on-java.org/hibernate-envers-getting-started/
https://www.baeldung.com/database-auditing-jpa
https://adamzareba.github.io/Audit-entities-with-Hibernate-Envers/
